/* IGC url browser SESSION BASED DB
   Building on marni/goigc/igc IGC parser
   Author Vanja Falck

Sources used to develop the code:

Testing for API in go-router:
https://github.com/appleboy/gofight

REST-API testing:
https://github.com/gavv/httpexpect

Security for memory ect:
https://github.com/awnumar/memguard

About GET, POST etc with gin router in golang:
https://github.com/gin-gonic/gin#using-get-post-put-patch-delete-and-options

IGC format and files: https://aerofiles.readthedocs.io/en/latest/guide/igc-writing.html
http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc

*/

package main

import (
  //"github.com/gin-gonic/gin"
  //	_ "github.com/heroku/x/hmetrics/onload"
  "net/http"
  "os"
  "log"
  //"go/build"
  "parag/parag"
)

//var Default build.Context = build.defaultContext()

func handlerRedirect (w http.ResponseWriter, r *http.Request){
  http.Redirect (w, r, "/igcinfo/api", 302)
}
func handlerNotFound (w http.ResponseWriter, r *http.Request){
  http.NotFound(w, r)
}

func main() {

  parag.GlobalDB  = &parag.TrackDB{}

  port := os.Getenv("PORT")
  // Do not use port on heroku!
  if port == "" {
  port= "8809"
  }

  parag.GlobalDB.Init()


/* Initiate with dummies to test:
s1 := RegTrack{TrID: 1, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Madrid%20to%20Jerez.igc", Track: Track{HDate: "2016-02-19 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}
s2 := RegTrack{TrID: 2, TrURL: "http://skypolaris.org/wp-content/uploads/IGS%20Files/Jarez%20to%20Senegal.igc", Track: Track{HDate: "2016-02-20 00:00:00 +0000 UTC", Pilot: "Miguel Angel Gordillo", Glider: "RV8", GliderID: "EC-XLL", TrackLength: 0}}
   GlobalDB.Add(s1)
   GlobalDB.Add(s2)
*/

    //port := os.Getenv("PORT")
    http.HandleFunc("/", handlerNotFound)
    http.HandleFunc("/igcinfo/", handlerRedirect)
    http.HandleFunc("/igcinfo/api/", parag.HandlerApiInfo)
    http.HandleFunc("/igcinfo/api/igc", parag.HandlerRegTrack)
    http.HandleFunc("/igcinfo/api/igc/", parag.HandlerRegSingleTrack)
  log.Fatal(http.ListenAndServe(":"+port, nil))
  //http.ListenAndServe("127.0.0.1:8809", nil)
}
