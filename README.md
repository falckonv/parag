# parag
Parag: Implementation of igc-online-file-reader with session memory. Not implemented: distance tracking, uptime. Testfile: currently not passing all types of wrong url status codes. Covering 58,6%. Deployment on Heroku using go mod init: https://paragliding-falckonv.herokuapp.com/igcinfo/api/igc


=== RUN   Test_addRegTrack
--- PASS: Test_addRegTrack (1.32s)
=== RUN   Test_multipleRegTracks
--- PASS: Test_multipleRegTracks (0.00s)
=== RUN   Test_handlerRegTrack_getAllTracks_empty
--- PASS: Test_handlerRegTrack_getAllTracks_empty (0.00s)
=== RUN   Test_handlerRegTrack_displayAllRegTracks
--- PASS: Test_handlerRegTrack_displayAllRegTracks (2.55s)
=== RUN   Test_HandlerRegSingleTrackGetSingleTrackMadrid
--- PASS: Test_HandlerRegSingleTrackGetSingleTrackMadrid (1.79s)
=== RUN   Test_handlerRegTrack_POST
--- PASS: Test_handlerRegTrack_POST (1.96s)
PASS
ok  	parag/parag	7.644s
